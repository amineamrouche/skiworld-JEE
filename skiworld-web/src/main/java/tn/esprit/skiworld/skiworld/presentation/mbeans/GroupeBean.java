package tn.esprit.skiworld.skiworld.presentation.mbeans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

//import org.primefaces.component.message.Message;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Client;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IChallengeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IClientServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IUserServiceLocal;

@ManagedBean(name = "adsBean")
@SessionScoped
public class GroupeBean {
	@EJB
	IGroupeServiceLocal gr;
	@EJB
	IUserServiceLocal us;
	@EJB
	IClientServiceLocal cl;

	@EJB
	IChallengeServiceLocal pew;

	static int ggaID;

	Integer groupeID;
	String groupename;
	String groupename1;
	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	// session.setAttribute("user", user);

	User us1;

	Date dateCreation;
	List<User> users;
	List<User> users2;
	List<User> oo = new ArrayList<User>();
	List<User> users1 = new ArrayList<User>();
	int idupdate;
	Groupe g1 = new Groupe();
	private UploadedFile file;
	String name;

	List<Challenge> jo = new ArrayList<Challenge>();
	List<Challenge> jo1 = new ArrayList<Challenge>();
	List<Challenge> jo2 = new ArrayList<Challenge>();
	List<Challenge> jo3 = new ArrayList<Challenge>();

	public IUserServiceLocal getUs() {
		return us;
	}

	public void setUs(IUserServiceLocal us) {
		this.us = us;
	}

	public IClientServiceLocal getCl() {
		return cl;
	}

	public void setCl(IClientServiceLocal cl) {
		this.cl = cl;
	}

	public IChallengeServiceLocal getPew() {
		return pew;
	}

	public void setPew(IChallengeServiceLocal pew) {
		this.pew = pew;
	}

	public List<Challenge> getJo() {
		return jo;
	}

	public void setJo(List<Challenge> jo) {
		this.jo = jo;
	}

	Groupe gga = new Groupe();

	List<Challenge> challenges;
	List<Groupe> grps = new ArrayList<Groupe>();
	List<Groupe> grps1 = new ArrayList<Groupe>();

	Groupe g = new Groupe();

	@PostConstruct
	public void init() {

		us1 = (User) session.getAttribute("user");
		// System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooo
		// "+us1.getFirstname());
		users = us.findAll(User.class);

		/*
		 * System.out.println("hablih"); for (User challenge : users) {
		 * if(challenge.getId()==us1.getId()){
		 * 
		 * users.remove(challenge); } }
		 */
		// grps = us.findById(User.class, 1).getGroupes();

	}

	public List<Groupe> Groupessssownedbyuser() {

		grps1 = us.findById(User.class, us1.getId()).getOwnedgroupes();

		return grps1;

	}

	public void DeleteGroupe(String i) {
		int j = Integer.parseInt(i);
		Groupe l = gr.findById(Groupe.class, j);
		gr.removeById(Groupe.class, l.getGroupeID());

	}

	public List<Groupe> Groupessssbyuser() {

		grps = gr.getGroupesbyuser(us1.getId());
		for (Groupe challenge : grps) {
			System.out.println("gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg     "
					+ challenge.getGroupename());
		}
		for (Challenge upp : us.findById(User.class, us1.getId()).getChallenges()) {
			System.out.println(upp.getNomChallenge());

		}
		return grps;

	}

	public void Addchallenge() throws IOException {

		FacesContext.getCurrentInstance().getExternalContext().redirect("GroupeSellectione.jsf");

	}

	public void adduserTogroupe(String i) {
		int j = Integer.parseInt(i);
		User l = us.findById(User.class, j);
		///// rod belek el owner lezmek trodou el user courant
		System.out.println(
				"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhjjjjjjjjjjjjjjj " + us1.getFirstname());
		// oo=new ArrayList<User>();
		// oo.add(us1);
		oo.add(l);
		g.setUsers(oo);
		g.setOwner(us1);
		oo.stream().forEach(System.out::println);

	}

	public Groupe findGroupByUser(String i) throws IOException {
		jo1.clear();
		jo2.clear();
		jo3.clear();
		// System.out.println("oooooooooooooooooooooooooooooooooooo"+us1.getFirstname());
		int j = Integer.parseInt(i);
		gga = gr.findById(Groupe.class, j);
		ggaID = gga.getGroupeID();
		FacesContext.getCurrentInstance().getExternalContext().redirect("GroupeSellectione.jsf");
		return gga;

	}

	public List<Challenge> findgroupebychallenge() {

		jo = pew.getchallengBygroupe(ggaID);
		return jo;

	}

	public List<Challenge> findgroupebychallenge1() {

		Date dd = new Date();

		jo = pew.getchallengBygroupe(ggaID);
		jo1 = new ArrayList<Challenge>();
		for (Challenge challenge : jo) {
			if (challenge.getDateExpiration().before(dd)) {
				jo1.add(challenge);
			}

		}
		return jo1;

	}

	public List<Challenge> findgroupebychallenge2() {

		Date dd = new Date();

		jo = pew.getchallengBygroupe(ggaID);
		jo2 = new ArrayList<Challenge>();
		for (Challenge challenge : jo) {
			if (challenge.getDateExpiration().after(dd) && challenge.getDateDebut().before(dd)) {
				jo2.add(challenge);
			}

		}
		return jo2;

	}

	public List<Challenge> findgroupebychallenge3() {

		Date dd = new Date();

		jo = pew.getchallengBygroupe(ggaID);
		jo3 = new ArrayList<Challenge>();
		for (Challenge challenge : jo) {
			if (challenge.getDateDebut().after(dd)) {
				jo3.add(challenge);
			}

		}
		return jo3;

	}

	public void doCreateGroupe() {

		g.setImage(name);
		g.setGroupename(groupename);
		g.setDateCreation(new Date());

		gr.add(g);
		oo = new ArrayList<User>();

	}

	public void redirecttoUpdate(String i) throws IOException {
		users2 = new ArrayList<User>();
		users2 = us.findAll(User.class);
		for (User challenge : users2) {
			System.out.println("pppppppppppppppppppppppppppppppppppppppppppp " + challenge.getFirstname());
		}

		int j = Integer.parseInt(i);
		idupdate = j;
		g1 = gr.findById(Groupe.class, idupdate);
		/*
		 * for (User challenge : users2) { for (User challenge1 : g1.getUsers())
		 * { if(challenge.getId()==challenge1.getId()){
		 * 
		 * users2.remove(challenge); for (User challenge2 : users2) {
		 * System.out.println("88888888888888888888888888888888888888888888888 "
		 * +challenge.getFirstname());
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * }
		 */
		groupename1 = g1.getGroupename();
		System.out.println("hetha el groupe esmou " + g1.getGroupename());
		FacesContext.getCurrentInstance().getExternalContext().redirect("UpdateGroupe.jsf");

	}

	public void adduserTogroupeupdate(String i) {
		FacesContext context = FacesContext.getCurrentInstance();
		int j = Integer.parseInt(i);
		User l = us.findById(User.class, j);
		List<Groupe> op = new ArrayList<Groupe>();
		List<User> op1 = new ArrayList<User>();
		op1 = g1.getUsers();

		op1.contains(l);
		op = l.getGroupes();
		if (op.size() == 0) {

			System.out.println(
					"****************************************************************************************************************************");

			op1.add(l);
			op1.stream().forEach(System.out::println);
			// for (User user : op1) {
			// System.out.println("this user is "+user.getFirstname());
			// }
			g1.setUsers(op1);
			// context.addMessage(null, new FacesMessage("Error", "You did
			// already participate"+ message) );
			context.addMessage(null,
					new FacesMessage("Success", "a new user has been successfully aded to this groupe"));
		} else {
			
				if (op1.contains(l)) {
					System.out.println("yaatek 3asba");
					context.addMessage(null, new FacesMessage("error", "this user already exists"));

				}

				else {
					System.out.println(
							"****************************************************************************************************************************");

					op1.add(l);
					op1.stream().forEach(e-> System.out.println("usernaaaaaaaaaaaaame "+e.getUsername()));
//					for (User user : op1) {
//						System.out.println("this user is " + user.getFirstname());
//					}
					g1.setUsers(op1);
					context.addMessage(null,
							new FacesMessage("Success", "a new user has been successfully aded to this groupe"));

				}
			

		}

	}

	public void updateGroupe() {

		g1.setGroupename(groupename1);
		gr.update(g1);
	}

	public void handleFileUpload(FileUploadEvent event) {
		name = event.getFile().getFileName();
		FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
		FacesContext.getCurrentInstance().addMessage(null, message);
		// C:\Users\amrouche\Documents\JEE\skiworld\skiworld-web\src\main\webapp\resources\img
		String localPath = "C:" + File.separator + "Users" + File.separator + "amrouche" + File.separator + "Documents"
				+ File.separator + "JEE" + File.separator + "skiworld" + File.separator + "skiworld-web"
				+ File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator
				+ "resources" + File.separator + "img" + File.separator + name;

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		String filepath = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "img"
				+ File.separator + name;

		try (

				InputStream input = event.getFile().getInputstream()) {
			OutputStream os = new FileOutputStream(localPath);
			OutputStream osServer = new FileOutputStream(localPath);
			byte[] b = new byte[2048];
			int length;

			while ((length = input.read(b)) != -1) {
				os.write(b, 0, length);
				osServer.write(b, 0, length);
			}

			input.close();
			os.close();
			osServer.close();

		} catch (IOException e) {
			// Show faces message?
		}

	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public IGroupeServiceLocal getGr() {
		return gr;
	}

	public void setGr(IGroupeServiceLocal gr) {
		this.gr = gr;
	}

	public Integer getGroupeID() {
		return groupeID;
	}

	public void setGroupeID(Integer groupeID) {
		this.groupeID = groupeID;
	}

	public String getGroupename() {
		return groupename;
	}

	public void setGroupename(String groupename) {
		this.groupename = groupename;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Challenge> getChallenges() {
		return challenges;
	}

	public void setChallenges(List<Challenge> challenges) {
		this.challenges = challenges;
	}

	public Groupe getG() {
		return g;
	}

	public void setG(Groupe g) {
		this.g = g;
	}

	public List<User> getOo() {
		return oo;
	}

	public void setOo(List<User> oo) {
		this.oo = oo;
	}

	public List<Groupe> getGrps() {
		return grps;
	}

	public void setGrps(List<Groupe> grps) {
		this.grps = grps;
	}

	public Groupe getGga() {
		return gga;
	}

	public void setGga(Groupe gga) {
		this.gga = gga;
	}

	public static int getGgaID() {
		return ggaID;
	}

	public static void setGgaID(int ggaID) {
		GroupeBean.ggaID = ggaID;
	}

	public List<Groupe> getGrps1() {
		return grps1;
	}

	public void setGrps1(List<Groupe> grps1) {
		this.grps1 = grps1;
	}

	public String getGroupename1() {
		return groupename1;
	}

	public void setGroupename1(String groupename1) {
		this.groupename1 = groupename1;
	}

	public List<User> getUsers1() {
		return users1;
	}

	public void setUsers1(List<User> users1) {
		this.users1 = users1;
	}

	public int getIdupdate() {
		return idupdate;
	}

	public void setIdupdate(int idupdate) {
		this.idupdate = idupdate;
	}

	public Groupe getG1() {
		return g1;
	}

	public void setG1(Groupe g1) {
		this.g1 = g1;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUs1() {
		return us1;
	}

	public void setUs1(User us1) {
		this.us1 = us1;
	}

	public List<Challenge> getJo1() {
		return jo1;
	}

	public void setJo1(List<Challenge> jo1) {
		this.jo1 = jo1;
	}

	public List<Challenge> getJo2() {
		return jo2;
	}

	public void setJo2(List<Challenge> jo2) {
		this.jo2 = jo2;
	}

	public List<Challenge> getJo3() {
		return jo3;
	}

	public void setJo3(List<Challenge> jo3) {
		this.jo3 = jo3;
	}

	public List<User> getUsers2() {
		return users2;
	}

	public void setUsers2(List<User> users2) {
		this.users2 = users2;
	}

}
