package tn.esprit.skiworld.skiworld.presentation.mbeans;


import java.io.Serializable;

import javax.ejb.EJB;
//import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import tn.esprit.skiworld.skiworld.entity.Client;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IUserServiceLocal;

@ManagedBean
@SessionScoped
public class UserMangementBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	IUserServiceLocal basicOpsLocal ;
	
	private Boolean loggedInAsAgent = false;
	private User user = new User();
	private boolean isLogged;

	public String logout() {
		isLogged = false;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login?faces-redirect=true";
	}
	public String doLogin() {
		String navigateTo = "";
		User userLoggedIn = basicOpsLocal.authentification(user.getUsername(), user.getPassword());
		if (userLoggedIn != null) {
			isLogged = true;
			user = userLoggedIn;
			HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute("user", user);
			
			User us1= (User)session.getAttribute("user");
			System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooo "+us1.getFirstname());
			if (userLoggedIn instanceof Client) {
				navigateTo = "/CrudGroupe?faces-redirect=true";
			}else {
				loggedInAsAgent = true;
				navigateTo = "/CrudChallenge?faces-redirect=true";
			
				}
		} else {
			loggedInAsAgent = false;
			navigateTo = "erreur?faces-redirect=true";
		
			System.err.println("not");
		}
		return navigateTo;
	}
	public IUserServiceLocal getBasicOpsLocal() {
		return basicOpsLocal;
	}
	public void setBasicOpsLocal(IUserServiceLocal basicOpsLocal) {
		this.basicOpsLocal = basicOpsLocal;
	}
	public Boolean getLoggedInAsAgent() {
		return loggedInAsAgent;
	}
	public void setLoggedInAsAgent(Boolean loggedInAsAgent) {
		this.loggedInAsAgent = loggedInAsAgent;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isLogged() {
		return isLogged;
	}
	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}


}
