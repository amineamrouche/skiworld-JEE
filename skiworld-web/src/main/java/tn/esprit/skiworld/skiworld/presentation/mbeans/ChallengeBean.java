package tn.esprit.skiworld.skiworld.presentation.mbeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IChallengeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IClientServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IUserServiceLocal;

@ManagedBean(name="ChalBean")
@SessionScoped
public class ChallengeBean {
	@EJB
	IGroupeServiceLocal gr ;
		@EJB
		IUserServiceLocal us ;
		@EJB
		IClientServiceLocal cl ;
		
		@EJB
		IChallengeServiceLocal chal ;
		
		private Integer idChallange;
		private Groupe idGroupe;
		private String nomChallenge;
		private String nomChallenge1;
private Date detedebut ;
private Date datedebut1 ;
		private Date dateCreation;
		private Date dateExpiration ;
		private Date dateExpiration1 ;

		private String descriptionChallenge;
		private String descriptionChallenge1;

		private List<User> challengers;
		private Integer nbrePlace;
		private Integer nbrePlace1;

		private String winner ;
		 private PieChartModel pieModel1;
		 Challenge ui = new Challenge();
		
		private List<User> llll = new ArrayList<User>();
		User k ;
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		//session.setAttribute("user", user);
		
		User us1;
		public List<User> getLlll() {
			return llll;
		}

		public void setLlll(List<User> llll) {
			this.llll = llll;
		}

		public User getK() {
			return k;
		}

		public void setK(User k) {
			this.k = k;
		}

		public Challenge getC() {
			return c;
		}

		public void setC(Challenge c) {
			this.c = c;
		}
		Challenge c = new Challenge();
		  List<Challenge> grps = new ArrayList<Challenge>();
		  Groupe ol  ;
		  List<User> gta = new ArrayList<User>();
		  Challenge c1 = new Challenge();
		  List<User> gta1 = new ArrayList<User>();
		  Challenge c2 = new Challenge();
		  BarChartModel barModel;
		@PostConstruct
		public void init(){
			
			//idGroupe=gr.findById(Groupe.class, GroupeBean.ggaID);
			//challengers=idGroupe.getUsers();
			//llll=gr.getUsersByGroupe(GroupeBean.ggaID);
			 us1= (User)session.getAttribute("user");
			 k = us.findById(User.class,us1.getId());
			
		}
		
public void initvariables(){
			
			idGroupe=gr.findById(Groupe.class, GroupeBean.ggaID);
			
			challengers=idGroupe.getUsers();
			llll=gr.getUsersByGroupe(GroupeBean.ggaID);
			// k = us.findById(User.class,1);
			
		}
public void Showdetailschallenge(String i) throws IOException{
	int j = Integer.parseInt(i);
	ui=chal.findById(Challenge.class, j);
    FacesContext.getCurrentInstance().getExternalContext().redirect("DetailsChallenge.jsf");

	
	
}
		
		public void DocreateChallenge() throws IOException{
			initvariables();
			// FacesContext.getCurrentInstance().getExternalContext().redirect("AjoutChallenge.jsf");
			 System.out.println("ffffffffffffffffffffffffffffffffffffffff"+idGroupe.getGroupename());
			c.setIdGroupe(idGroupe);
			c.setNomChallenge(nomChallenge);
			c.setDateCreation(new Date());
			c.setDateExpiration(dateExpiration);
			c.setDateDebut(detedebut);
			c.setDescriptionChallenge(descriptionChallenge);
			
			c.setChallengers(llll);
			c.setNbrePlace(nbrePlace);
			c.setChallengeowner(k);
		/*	List<Challenge> lol = new ArrayList<Challenge>();
			lol.add(c);
			for (User user : llll) {
				user.setChallenges(lol);
				for (Challenge challenge : user.getChallenges()) {
					System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo      "+challenge.getNomChallenge());
				}
				us.update(user);
				
			}*/
			if(c.getDateExpiration().before(c.getDateDebut())){
				
				//System.out.println("fuck you");
			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "please check your dates "));
//warn();
				    
			}else{
			chal.add(c);
		}
			for (User user1 : idGroupe.getUsers()) {
				us.send(user1,idGroupe);
			}
			
			}
		
		public void warn() {
		    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Watch out for PrimeFaces."));
		}	
		
		public List<Challenge> Groupessssownedbyuser(){
			
			grps = chal.getChallengeByowner(k.getId());

			return grps ;
			
		}
		
		public void DeleteChallenge(String i){
			int j= Integer.parseInt(i); 
			Challenge l = chal.findById(Challenge.class, j);
			System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk      "+l.getIdChallange());
			//chal.removeById(Challenge.class, l.getIdChallange());
			
			chal.deletechallenge(l.getIdChallange());
			
			System.out.println("done");

			
			
		}
		
		public void redirecttoUpdate(String i) throws IOException{
			
			int j= Integer.parseInt(i); 
			c1=chal.findById(Challenge.class, j);
			
			ol=gr.findById(Groupe.class, c1.getIdGroupe().getGroupeID());
			gta=ol.getUsers();
			nomChallenge1=c1.getNomChallenge();
			descriptionChallenge1=c1.getDescriptionChallenge();
			dateExpiration1=c1.getDateExpiration();
			datedebut1=c1.getDateDebut();
			nbrePlace1=c1.getNbrePlace();
			
			
		   
		   // System.out.println("hetha el groupe esmou "+g1.getGroupename());
		    FacesContext.getCurrentInstance().getExternalContext().redirect("UpdateC.jsf");


				}
		
		public void DoupdateChallenge() throws IOException{
			
			// FacesContext.getCurrentInstance().getExternalContext().redirect("AjoutChallenge.jsf");
		//	c1.setIdGroupe(idGroupe);
			c1.setNomChallenge(nomChallenge1);
			//c.setDateCreation(new Date());
			c1.setDateExpiration(dateExpiration1);
			c1.setDateDebut(datedebut1);
			c1.setDescriptionChallenge(descriptionChallenge1);
			
			//c.setChallengers(llll);
			c1.setNbrePlace(nbrePlace1);
			//c.setChallengeowner(k);
		/*	List<Challenge> lol = new ArrayList<Challenge>();
			lol.add(c);
			for (User user : llll) {
				user.setChallenges(lol);
				for (Challenge challenge : user.getChallenges()) {
					System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo      "+challenge.getNomChallenge());
				}
				us.update(user);
				
			}*/
			
			chal.update(c1);
		}
	public void redirecttoWinner(String i) throws IOException{
			
			int j= Integer.parseInt(i); 
			c2=chal.findById(Challenge.class, j);
			gta1=c2.getChallengers();
			
			
			
		   
		   // System.out.println("hetha el groupe esmou "+g1.getGroupename());
		    FacesContext.getCurrentInstance().getExternalContext().redirect("setwinner.jsf");


				}
	public void putuseraswinner(String i) throws IOException{
		
		int j= Integer.parseInt(i); 
		User ul = us.findById(User.class, j);
		c2.setWinner(ul);
		chal.update(c2);
		
		
		
		
	   
	   // System.out.println("hetha el groupe esmou "+g1.getGroupename());
	    FacesContext.getCurrentInstance().getExternalContext().redirect("setwinner.jsf");


			}
	  public void createPieModel() throws IOException {
		   FacesContext.getCurrentInstance().getExternalContext().redirect("showpie.jsf");
		  List<Challenge> ho = chal.getallchallengByuser(1);
		  List<Challenge> ho1 = chal.getsumchallengesgagnerByuser(1);

		  int a = ho.size();
		  int b = ho1.size();
		  int c = a-b ;
			System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo      "+a);
			System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo      "+b);
			System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo      "+c);

	        pieModel1 = new PieChartModel();
	         
	        pieModel1.set("challenges won", b);
	        pieModel1.set("challenges lost ", c);
	       
	         
	        pieModel1.setTitle("Challenges statistique");
	        pieModel1.setLegendPosition("w");
	        ////////////////////////////////////////////////////////////////////////////////////////
	        List<Groupe> cc=chal.getallgroupesbyuser(1);
			  List<Challenge>cc1 = chal.getallchallengesowned(1);
			  List<Challenge>cc2 = chal.getsumchallengesgagnerByuser(1);

			  
			  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc.size());
			  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc1.size());
			  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc2.size());


			   barModel = new BarChartModel();
			  
		        ChartSeries boys = new ChartSeries();
		        boys.setLabel("owned");
		        boys.set("groupes owned", cc.size());
		        boys.set("challenges owned", cc1.size());
		        boys.set("challenges won", cc2.size());
		      
		 
		 barModel.setAnimate(true);
		        barModel.addSeries(boys);
		      
		         
		        barModel.setTitle("Bar Chart");
		        barModel.setLegendPosition("ne");
		         
		        Axis xAxis = barModel.getAxis(AxisType.X);
		        xAxis.setLabel("owned");
		         
		        Axis yAxis = barModel.getAxis(AxisType.Y);
		        yAxis.setLabel("number");
		        yAxis.setMin(0);
		        yAxis.setMax(10);
	    }
		
	  public void createBarModel() {
		  List<Groupe> cc=chal.getallgroupesbyuser(1);
		  List<Challenge>cc1 = chal.getallchallengesowned(1);
		  List<Challenge>cc2 = chal.getsumchallengesgagnerByuser(1);

		  
		  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc.size());
		  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc1.size());
		  System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh "+cc2.size());


		   barModel = new BarChartModel();
		  
	        ChartSeries boys = new ChartSeries();
	        boys.setLabel("owned");
	        boys.set("groupes owned", cc.size());
	        boys.set("challenges owned", cc1.size());
	        boys.set("challenges won", cc2.size());
	      
	 
	 
	        barModel.addSeries(boys);
	      
	         
	        barModel.setTitle("Bar Chart");
	        barModel.setLegendPosition("ne");
	         
	        Axis xAxis = barModel.getAxis(AxisType.X);
	        xAxis.setLabel("owned");
	         
	        Axis yAxis = barModel.getAxis(AxisType.Y);
	        yAxis.setLabel("number");
	        yAxis.setMin(0);
	        yAxis.setMax(100);
	    }
		
		public IGroupeServiceLocal getGr() {
			return gr;
		}
		public void setGr(IGroupeServiceLocal gr) {
			this.gr = gr;
		}
		public IUserServiceLocal getUs() {
			return us;
		}
		public void setUs(IUserServiceLocal us) {
			this.us = us;
		}
		public IClientServiceLocal getCl() {
			return cl;
		}
		public void setCl(IClientServiceLocal cl) {
			this.cl = cl;
		}
		public IChallengeServiceLocal getChal() {
			return chal;
		}
		public void setChal(IChallengeServiceLocal chal) {
			this.chal = chal;
		}
		public Integer getIdChallange() {
			return idChallange;
		}
		public void setIdChallange(Integer idChallange) {
			this.idChallange = idChallange;
		}
		public Groupe getIdGroupe() {
			return idGroupe;
		}
		public void setIdGroupe(Groupe idGroupe) {
			this.idGroupe = idGroupe;
		}
		public String getNomChallenge() {
			return nomChallenge;
		}
		public void setNomChallenge(String nomChallenge) {
			this.nomChallenge = nomChallenge;
		}
		public Date getDateCreation() {
			return dateCreation;
		}
		public void setDateCreation(Date dateCreation) {
			this.dateCreation = dateCreation;
		}
		public Date getDateExpiration() {
			return dateExpiration;
		}
		public void setDateExpiration(Date dateExpiration) {
			this.dateExpiration = dateExpiration;
		}
		public String getDescriptionChallenge() {
			return descriptionChallenge;
		}
		public void setDescriptionChallenge(String descriptionChallenge) {
			this.descriptionChallenge = descriptionChallenge;
		}
		public List<User> getChallengers() {
			return challengers;
		}
		public void setChallengers(List<User> challengers) {
			this.challengers = challengers;
		}
		public Integer getNbrePlace() {
			return nbrePlace;
		}
		public void setNbrePlace(Integer nbrePlace) {
			this.nbrePlace = nbrePlace;
		}
		public String getWinner() {
			return winner;
		}
		public void setWinner(String winner) {
			this.winner = winner;
		}

		public String getNomChallenge1() {
			return nomChallenge1;
		}

		public void setNomChallenge1(String nomChallenge1) {
			this.nomChallenge1 = nomChallenge1;
		}

		public Date getDateExpiration1() {
			return dateExpiration1;
		}

		public void setDateExpiration1(Date dateExpiration1) {
			this.dateExpiration1 = dateExpiration1;
		}

		public String getDescriptionChallenge1() {
			return descriptionChallenge1;
		}

		public void setDescriptionChallenge1(String descriptionChallenge1) {
			this.descriptionChallenge1 = descriptionChallenge1;
		}

		public Integer getNbrePlace1() {
			return nbrePlace1;
		}

		public void setNbrePlace1(Integer nbrePlace1) {
			this.nbrePlace1 = nbrePlace1;
		}

		public List<Challenge> getGrps() {
			return grps;
		}

		public void setGrps(List<Challenge> grps) {
			this.grps = grps;
		}

		public Groupe getOl() {
			return ol;
		}

		public void setOl(Groupe ol) {
			this.ol = ol;
		}

		public List<User> getGta() {
			return gta;
		}

		public void setGta(List<User> gta) {
			this.gta = gta;
		}

		public Challenge getC1() {
			return c1;
		}

		public void setC1(Challenge c1) {
			this.c1 = c1;
		}

		public List<User> getGta1() {
			return gta1;
		}

		public void setGta1(List<User> gta1) {
			this.gta1 = gta1;
		}

		public Challenge getC2() {
			return c2;
		}

		public void setC2(Challenge c2) {
			this.c2 = c2;
		}

		public PieChartModel getPieModel1() {
			return pieModel1;
		}

		public void setPieModel1(PieChartModel pieModel1) {
			this.pieModel1 = pieModel1;
		}

		public BarChartModel getBarModel() {
			return barModel;
		}

		public void setBarModel(BarChartModel barModel) {
			this.barModel = barModel;
		}

		public HttpSession getSession() {
			return session;
		}

		public void setSession(HttpSession session) {
			this.session = session;
		}

		public User getUs1() {
			return us1;
		}

		public void setUs1(User us1) {
			this.us1 = us1;
		}

		public Date getDetedebut() {
			return detedebut;
		}

		public void setDetedebut(Date detedebut) {
			this.detedebut = detedebut;
		}

		public Date getDatedebut1() {
			return datedebut1;
		}

		public void setDatedebut1(Date datedebut1) {
			this.datedebut1 = datedebut1;
		}

		public Challenge getUi() {
			return ui;
		}

		public void setUi(Challenge ui) {
			this.ui = ui;
		}
		
		
		
		
		
		
		
		
}
