package resources;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;

@Path(value="Jobs")
@RequestScoped
public class JobVolenteerResource {
	
	@EJB
	IGroupeServiceLocal jobsServiceRest ;
	
	
	
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRest()
	{
		
		return Response.ok(jobsServiceRest.findAll(Groupe.class)).build(); 
	}
}
