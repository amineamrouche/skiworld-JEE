package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;
@Remote
public interface IUserServiceRemote extends IBasicOpsRemote<User> {
	public User authentification(String username, String pwd);
	public void send(User u , Groupe g);

}
