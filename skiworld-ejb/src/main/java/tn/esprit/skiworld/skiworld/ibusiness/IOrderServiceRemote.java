package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Orders;
import tn.esprit.skiworld.skiworld.entity.User;
@Remote
public interface IOrderServiceRemote extends IBasicOpsRemote<Orders> {
	public Orders findCart(User user);
}
