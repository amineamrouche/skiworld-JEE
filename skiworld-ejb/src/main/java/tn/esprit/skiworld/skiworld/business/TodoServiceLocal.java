package tn.esprit.skiworld.skiworld.business;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Todo;

@Local
public interface TodoServiceLocal {
	
	void create(Todo todo);
	List<Todo> findAll();

}
