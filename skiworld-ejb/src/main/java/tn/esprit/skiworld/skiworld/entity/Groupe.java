package tn.esprit.skiworld.skiworld.entity;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Groupe
 *
 */
@Entity
public class Groupe implements Serializable {

	
	private Integer groupeID;
	private String groupename;
	private Date dateCreation;
	private User owner ;
	private String Image ;
	private List<User> users;
	private List<Challenge> challenges ;
	@OneToMany(mappedBy="idGroupe",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	public List<Challenge> getChallenges() {
		return challenges;
	}
	public void setChallenges(List<Challenge> challenges) {
		this.challenges = challenges;
	}

	private static final long serialVersionUID = 1L;

	public Groupe() {
		super();
	}   
	@Id   
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getGroupeID() {
		return this.groupeID;
	}

	public void setGroupeID(Integer groupeID) {
		this.groupeID = groupeID;
	}   
	public String getGroupename() {
		return this.groupename;
	}

	public void setGroupename(String groupename) {
		this.groupename = groupename;
	}   
	public Date getDateCreation() {
		return this.dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}   
	@ManyToMany(fetch = FetchType.EAGER)
	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	@ManyToOne
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
   
}
