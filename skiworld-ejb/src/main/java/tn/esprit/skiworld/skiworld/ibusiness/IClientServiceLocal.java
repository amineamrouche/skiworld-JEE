package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Client;

@Local
public interface IClientServiceLocal extends IBasicOpsLocal<Client> {

}
