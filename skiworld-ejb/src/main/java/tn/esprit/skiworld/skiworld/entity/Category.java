package tn.esprit.skiworld.skiworld.entity;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity

public class Category implements Serializable {

	
	private int id;
	private String name;
	private String img;
	
	private static final long serialVersionUID = 1L;

	public Category() {
		super();
	}   
	public Category(String name, String img) {
		super();
		this.name = name;
		this.img = img;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	

   
}
