package tn.esprit.skiworld.skiworld.business;

import javax.ejb.Stateless;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.ibusiness.ICategoryServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.ICategoryServiceRemote;
@Stateless
public class CategoryServiceImpl extends BasicOpsImpl<Category> implements ICategoryServiceLocal, ICategoryServiceRemote {

}
