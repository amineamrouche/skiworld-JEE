package tn.esprit.skiworld.skiworld.ibusiness;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;

@Local
public interface IGroupeServiceLocal extends IBasicOpsLocal<Groupe> {
	public List<User> getUsersByGroupe(int i);
	public List<Groupe> getGroupesbyuser(int i);
	

}
