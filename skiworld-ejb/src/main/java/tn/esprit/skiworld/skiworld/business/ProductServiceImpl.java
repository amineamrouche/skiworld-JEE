package tn.esprit.skiworld.skiworld.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.entity.Product;
import tn.esprit.skiworld.skiworld.ibusiness.IProductServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IProductServiceRempte;
@Stateless
public class ProductServiceImpl extends BasicOpsImpl<Product> implements IProductServiceLocal, IProductServiceRempte {

	@Override
	public List<Product> findByCatgory(Category c) {
		String sql ="from Bordereau"
				+ " where id_category="+c.getId();
		Query query = this.entityManager.createQuery(sql);
		return query.getResultList();
	}

} 
