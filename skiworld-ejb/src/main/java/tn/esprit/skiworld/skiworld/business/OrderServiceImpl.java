package tn.esprit.skiworld.skiworld.business;

import javax.ejb.Stateless;
import javax.persistence.Query;

import tn.esprit.skiworld.skiworld.entity.Orders;
import tn.esprit.skiworld.skiworld.entity.Status;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IOrderServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IOrderServiceRemote;
@Stateless
public class OrderServiceImpl extends BasicOpsImpl<Orders> implements IOrderServiceLocal, IOrderServiceRemote {

	@Override
	public Orders findCart(User user) {
		String sql = "FROM Orders WHERE status = 4";
		Query query = this.entityManager.createQuery(sql);
		return (Orders) query.getSingleResult();
	}

}
