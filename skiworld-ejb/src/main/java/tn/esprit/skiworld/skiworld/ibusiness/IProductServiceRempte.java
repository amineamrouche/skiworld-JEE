package tn.esprit.skiworld.skiworld.ibusiness;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.entity.Product;
@Remote
public interface IProductServiceRempte extends IBasicOpsRemote<Product> {
	public List<Product> findByCatgory(Category c);
}
