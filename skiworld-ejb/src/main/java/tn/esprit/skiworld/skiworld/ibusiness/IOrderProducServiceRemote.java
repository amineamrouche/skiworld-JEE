package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Remote;
import javax.persistence.criteria.Order;

import tn.esprit.skiworld.skiworld.business.OrderServiceImpl;
import tn.esprit.skiworld.skiworld.entity.OrderProduct;
import tn.esprit.skiworld.skiworld.entity.Product;
@Remote
public interface IOrderProducServiceRemote extends IBasicOpsRemote<OrderProduct> {
	public void addToCart(Order order, Product product);
}
