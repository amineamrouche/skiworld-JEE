package tn.esprit.skiworld.skiworld.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Order
 *
 */
@Entity

public class Orders implements Serializable {

	
	private int id;
	private Status status;
	private List<OrderProduct> products;
	private User buyer;
	private static final long serialVersionUID = 1L;

	public Orders() {
		super();
	}   
	
	
	public Orders(Status status) {
		super();
		this.status = status;
	}


	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	@OneToMany(mappedBy="order")
	public List<OrderProduct> getProducts() {
		return products;
	}
	public void setProducts(List<OrderProduct> products) {
		this.products = products;
	}
	@ManyToOne
	public User getBuyer() {
		return buyer;
	}
	public void setBuyer(User buyer) {
		this.buyer = buyer;
	}


	@Override
	public String toString() {
		return "Orders [id=" + id + ", status=" + status + "]";
	}
   
}
