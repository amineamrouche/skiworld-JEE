package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Client;

@Remote
public interface IClientServiceRemote extends IBasicOpsRemote<Client> {

}
