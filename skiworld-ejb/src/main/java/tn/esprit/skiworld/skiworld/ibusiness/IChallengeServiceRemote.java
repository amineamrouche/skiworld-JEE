package tn.esprit.skiworld.skiworld.ibusiness;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Groupe;

@Remote
public interface IChallengeServiceRemote extends IBasicOpsRemote<Challenge> {
	public void addUserToChallnge(Challenge chal,int idUser);
	public void setUserAsWinner(Challenge chal,int idUser);
	public void placesdecrement(Challenge chal , int idUser);
	public List<Challenge> getchallengBygroupe(int id);
	public List<Challenge> getChallengeByowner(int i);
	public void deletechallenge(int i);
	public List<Challenge> getallchallengByuser(int id);
	public List<Challenge> getsumchallengesgagnerByuser(int id);
	public List<Challenge> getallchallengesowned(int id);
	public List<Groupe> getallgroupesbyuser(int id);

	
}
