package tn.esprit.skiworld.skiworld.entity;

import java.io.Serializable;
import java.lang.String;

import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity(name="T_user")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class User implements Serializable {
    private int id;
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private List<Address> addresses;
	private List<Groupe> groupes;
	private List<Challenge> challenges ;
	private List<Groupe> ownedgroupes;
	private List<Challenge> ownedChallenges;
	private List<Challenge> Challengeswon;
    private String Email ;


	
	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}   
	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}   


	
	@OneToMany(fetch = FetchType.EAGER)
	public List<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}   
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}   
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@ManyToMany(mappedBy="challengers",fetch = FetchType.EAGER)
	public List<Challenge> getChallenges() {
		return challenges;
	}
	public void setChallenges(List<Challenge> challenges) {
		this.challenges = challenges;
	}
@ManyToMany(mappedBy="users",fetch = FetchType.EAGER)
	public List<Groupe> getGroupes() {
		return groupes;
	}
	public void setGroupes(List<Groupe> groupes) {
		this.groupes = groupes;
	}
	@OneToMany(mappedBy="owner",fetch=FetchType.EAGER)
	public List<Groupe> getOwnedgroupes() {
		return ownedgroupes;
	}
	public void setOwnedgroupes(List<Groupe> ownedgroupes) {
		this.ownedgroupes = ownedgroupes;
	}
	@OneToMany(mappedBy="challengeowner",fetch=FetchType.EAGER)
	public List<Challenge> getOwnedChallenges() {
		return ownedChallenges;
	}
	public void setOwnedChallenges(List<Challenge> ownedChallenges) {
		this.ownedChallenges = ownedChallenges;
	}
	@OneToMany(mappedBy="winner",fetch=FetchType.EAGER)
	public List<Challenge> getChallengeswon() {
		return Challengeswon;
	}
	public void setChallengeswon(List<Challenge> challengeswon) {
		Challengeswon = challengeswon;
	}
	
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
