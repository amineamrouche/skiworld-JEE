package tn.esprit.skiworld.skiworld.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.Product;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.ICategoryServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.ICategoryServiceRemote;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceRemote;

@Stateless
public class GroupeService extends BasicOpsImpl<Groupe> implements IGroupeServiceLocal, IGroupeServiceRemote{
	BasicOpsImpl<User> k =  new BasicOpsImpl<User>();

	public List<User> findUsersBygroupe(User c) {
		
		String sql ="select users_id from user_groupe"
				+ " where groupes_groupeID="+c.getId();
		Query query = this.entityManager.createQuery(sql);
		return query.getResultList();
	}

	public void addUserToGroupe(int c, int g) {
		Groupe gr=findById(null, c);
		User us = k.findById(null, g);
		
		gr.getUsers().add(us);
		
		us.getGroupes().add(gr);
		
		
	}

	@Override
	public List<User> getUsersByGroupe(int i) {
		String sql ="select c.users from Groupe c"
				+ " where c.groupeID=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", i);
		return query.getResultList();
	}

	@Override
	public List<Groupe> getGroupesbyuser(int i) {
		String sql ="select c.groupes from T_user c"
				+ " where c.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", i);
		return query.getResultList();
	}

	
}
