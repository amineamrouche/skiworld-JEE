package tn.esprit.skiworld.skiworld.business;

import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.Query;

import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceRemote;
import tn.esprit.skiworld.skiworld.ibusiness.IUserServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IUserServiceRemote;

@Stateless
public class UserService extends BasicOpsImpl<User> implements IUserServiceLocal, IUserServiceRemote {

	@Override
	public User authentification(String username, String pwd) {
		String jpql = "SELECT z FROM T_user z WHERE z.username=:username and z.password=:pwd";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("username", username);
		query.setParameter("pwd", pwd);
		User user = null;
		try {
			user = (User) query.getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
		}

		return user;
	}

	@Override
	public void send(User u, Groupe g) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		final String username = "ahmed.ben.othmenne@gmail.com";
		final String password = "123abz123";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		Message message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress("ahmed.ben.othmenne@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("" + u.getEmail()));

			message.setSubject("From Skiworld");

			message.setText("a new challenge has been added to this groupe :" + g.getGroupename());

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException ex) {

		}

	}

}
