package tn.esprit.skiworld.skiworld.business;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Todo;

@Remote
public interface TodoServiceRemote {
	public void create(Todo todo);
}
