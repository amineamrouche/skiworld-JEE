package tn.esprit.skiworld.skiworld.business;

import javax.ejb.Stateless;

import tn.esprit.skiworld.skiworld.entity.Client;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.ibusiness.IClientServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IClientServiceRemote;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IGroupeServiceRemote;

@Stateless
public class ClientService extends BasicOpsImpl<Client> implements IClientServiceLocal, IClientServiceRemote{

}
