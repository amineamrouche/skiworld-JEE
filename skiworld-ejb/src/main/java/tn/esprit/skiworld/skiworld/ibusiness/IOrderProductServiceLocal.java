package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Local;
import javax.persistence.criteria.Order;

import tn.esprit.skiworld.skiworld.entity.OrderProduct;
import tn.esprit.skiworld.skiworld.entity.Product;
@Local
public interface IOrderProductServiceLocal extends IBasicOpsLocal<OrderProduct> {
	public void addToCart(Order order, Product product);
}
