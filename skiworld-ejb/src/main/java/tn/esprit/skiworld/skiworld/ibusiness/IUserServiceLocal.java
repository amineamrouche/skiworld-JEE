package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.Product;
import tn.esprit.skiworld.skiworld.entity.User;
@Local
public interface IUserServiceLocal extends IBasicOpsLocal<User> {
	public User authentification(String username, String pwd);
	public void send(User u , Groupe g);

}
