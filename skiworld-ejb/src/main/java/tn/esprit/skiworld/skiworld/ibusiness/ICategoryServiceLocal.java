package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Category;

@Local
public interface ICategoryServiceLocal extends IBasicOpsLocal<Category> {

}
