package tn.esprit.skiworld.skiworld.entity;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Challenge
 *
 */
@Entity

public class Challenge implements Serializable {

	
	private Integer idChallange;
	private Groupe idGroupe;
	private String nomChallenge;
	private Date dateCreation;
	private Date dateDebut;
	private Date dateExpiration ;
	private String descriptionChallenge;
	private List<User> challengers;
	private Integer nbrePlace;
	private User winner ;
	private User challengeowner ;
	private static final long serialVersionUID = 1L;

	public Challenge() {
		super();
	}   
	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getIdChallange() {
		return this.idChallange;
	}

	public void setIdChallange(Integer idChallange) {
		this.idChallange = idChallange;
	}   
	@ManyToOne
	public Groupe getIdGroupe() {
		return this.idGroupe;
	}

	public void setIdGroupe(Groupe idGroupe) {
		this.idGroupe = idGroupe;
	}   
	public String getNomChallenge() {
		return this.nomChallenge;
	}

	public void setNomChallenge(String nomChallenge) {
		this.nomChallenge = nomChallenge;
	}   
	public Date getDateCreation() {
		return this.dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}   
	public String getDescriptionChallenge() {
		return this.descriptionChallenge;
	}

	public void setDescriptionChallenge(String descriptionChallenge) {
		this.descriptionChallenge = descriptionChallenge;
	}  
	
@ManyToMany(fetch = FetchType.EAGER)
	public List<User> getChallengers() {
		return this.challengers;
	}

	public void setChallengers(List<User> challengers) {
		this.challengers = challengers;
	}   
	public Integer getNbrePlace() {
		return this.nbrePlace;
	}

	public void setNbrePlace(Integer nbrePlace) {
		this.nbrePlace = nbrePlace;
	}
	public Date getDateExpiration() {
		return dateExpiration;
	}
	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}
	@ManyToOne
	public User getWinner() {
		return winner;
	}
	public void setWinner(User winner) {
		this.winner = winner;
	}
	@ManyToOne
	public User getChallengeowner() {
		return challengeowner;
	}
	public void setChallengeowner(User challengeowner) {
		this.challengeowner = challengeowner;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	
   
}
