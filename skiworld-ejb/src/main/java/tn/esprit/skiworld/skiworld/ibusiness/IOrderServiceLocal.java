package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Orders;
import tn.esprit.skiworld.skiworld.entity.User;
@Local
public interface IOrderServiceLocal extends IBasicOpsLocal<Orders> {
	public Orders findCart(User user);
}
