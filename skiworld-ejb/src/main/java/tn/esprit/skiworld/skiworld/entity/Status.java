package tn.esprit.skiworld.skiworld.entity;

public enum Status {
	CART, PREPARATON_IN_PROGRESS, CANCELED, SHIPPED, DELIVERED
}
