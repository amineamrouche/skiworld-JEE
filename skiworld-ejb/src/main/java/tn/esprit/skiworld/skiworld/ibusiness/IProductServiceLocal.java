package tn.esprit.skiworld.skiworld.ibusiness;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.entity.Product;
@Local
public interface IProductServiceLocal extends IBasicOpsLocal<Product> {
	public List<Product> findByCatgory(Category c);
}
