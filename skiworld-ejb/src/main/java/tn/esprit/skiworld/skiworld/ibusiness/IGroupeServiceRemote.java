package tn.esprit.skiworld.skiworld.ibusiness;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Category;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.User;

@Remote
public interface IGroupeServiceRemote extends IBasicOpsRemote<Groupe> {
	public List<User> getUsersByGroupe(int i);
	public List<Groupe> getGroupesbyuser(int i);

}
