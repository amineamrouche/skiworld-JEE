package tn.esprit.skiworld.skiworld.ibusiness;

import javax.ejb.Remote;

import tn.esprit.skiworld.skiworld.entity.Category;

@Remote
public interface ICategoryServiceRemote extends IBasicOpsRemote<Category> {

}
