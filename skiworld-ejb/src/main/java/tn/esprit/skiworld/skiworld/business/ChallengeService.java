package tn.esprit.skiworld.skiworld.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import tn.esprit.skiworld.skiworld.entity.Challenge;
import tn.esprit.skiworld.skiworld.entity.Groupe;
import tn.esprit.skiworld.skiworld.entity.Orders;
import tn.esprit.skiworld.skiworld.entity.User;
import tn.esprit.skiworld.skiworld.ibusiness.IChallengeServiceLocal;
import tn.esprit.skiworld.skiworld.ibusiness.IChallengeServiceRemote;

@Stateless
public class ChallengeService extends BasicOpsImpl<Challenge>  implements IChallengeServiceLocal, IChallengeServiceRemote {

	@Override
	public void addUserToChallnge(Challenge chal, int idUser) {
		UserService us = new UserService();
		User u =us.findById(User.class, idUser);
	//	GroupeService gr = new GroupeService();
		//Groupe g =gr.findById(Groupe.class, idGroupe);
		
		u.getChallenges().add(chal);
		
		chal.getChallengers().add(u);
		
	}

	@Override
	public void setUserAsWinner(Challenge chal, int idUser) {
		// TODO Auto-generated method stub
		UserService us = new UserService();
		User u =us.findById(User.class, idUser);
	//	GroupeService gr = new GroupeService();
		//Groupe g =gr.findById(Groupe.class, idGroupe)
		
		//chal.setWinner(u.getFirstname());
		
	}

	@Override
	public void placesdecrement(Challenge chal, int idUser) {
		// TODO Auto-generated method stub
		
		
		
}

	@Override
	public List<Challenge> getchallengBygroupe(int id) {
		// TODO Auto-generated method stub
		
		String sql ="select c from Challenge c"
				+ " where c.idGroupe.groupeID=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<Challenge> getChallengeByowner(int i) {
		String sql ="select c from Challenge c"
				+ " where c.challengeowner.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", i);
		return query.getResultList();
	}

	@Override
	public void deletechallenge(int i) {
		String sql ="delete from Challenge c"
				+ " where c.idChallange=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", i);
		query.executeUpdate();
	}

	@Override
	public List<Challenge> getallchallengByuser(int id) {
		String sql ="select c.challenges from T_user c"
				+ " where c.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<Challenge> getsumchallengesgagnerByuser(int id) {
		String sql ="select c from Challenge c"
				+ " where c.winner.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", id);
		return query.getResultList();
	}

	

	@Override
	public List<Challenge> getallchallengesowned(int id) {
		String sql ="select c from Challenge c"
				+ " where c.challengeowner.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<Groupe> getallgroupesbyuser(int id){
		String sql ="select c from Groupe c"
				+ " where c.owner.id=:id";
		Query query = this.entityManager.createQuery(sql).setParameter("id", id);
		return query.getResultList();
		
	}

	
	
	
	
	
	



	
}
